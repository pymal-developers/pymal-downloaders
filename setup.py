from setuptools import setup


# Dynamically calculate the version based on pymal-downloaders.VERSION.
version = __import__('pymal-downloaders').get_version()


setup(
    name='pymal-downloaders',
    packages=['pymal-downloaders'],
    version=version,
    description='A plugin for pymal for fast downloads.',
    author='pymal-developers',
    license="BSD",
    url='https://bitbucket.org/pymal-developers/pymal-downloaders/',
    keywords=["MyAnimeList", "MAL", "pymal", 'downloads'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Natural Language :: Japanese',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.0',
        'Programming Language :: Python :: 3.1',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Database',
        'Topic :: Database :: Front-Ends',
        'Topic :: Home Automation',
        'Topic :: Internet',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: User Interfaces',
    ],
    install_requires=[
        'pymal>=0.5b2',
    ],
)
