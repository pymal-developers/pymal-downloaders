[TOC]

pymal
=====
If you read this that mean that you want to do something with the code (copy, read or add).
This README will try to explain how everything is organized and how it should look like.

Usage
=====
Never put tests in here!
That's why we have a folder for them :)

Object files
------------
 * Each object should be placed in his own file.
 * If some classes should look the same, try to make the same interface in functions' names and their arguments.
 * Inherited is good if you can pass code.
 * Remember that we are reading from anther server (myanimelist.net).
    Make everything as lazy as possible and use all the information from each data you receive.

Objects
-------
###Downloader
###DirectDownloader
###TorrentDownloader
###RssDownloader
