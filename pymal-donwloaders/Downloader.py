__authors__ = ""
__copyright__ = "(c) 2014, pymal"
__license__ = "BSD License"
__contact__ = "Name Of Current Guardian of this file <email@address>"

from pymal import decorators


class Downloader(object, metaclass=decorators.Singleton):
    def download(self, anime_name: str, resolution: int):
        raise NotImplemented()


# -------------------- DirectDownloader section --------------------
class DirectDownloader(object, metaclass=decorators.Singleton):
    pass


# -------------------- TorrentDownloader section --------------------
class TorrentDownloader(object, metaclass=decorators.Singleton):
    pass


# -------------------- RssDownloader section --------------------
class RssDownloader(Downloader):
    pass
